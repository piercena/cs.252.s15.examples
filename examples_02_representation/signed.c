#include <stdio.h>

typedef unsigned int uint;

int
main(int argc, char *argv[])
{
    int i0  = -1;
    uint i1 = -1;

    printf("i0      = %d\n"  , i0);
    printf("i0      = %u\n"  , i0);
    printf("i0      = 0x%x\n", i0);
    
    printf("i1      = %d\n"  , i1);
    printf("i1      = %u\n"  , i1);
    printf("i1      = 0x%x\n", i1);

    printf("i0 & i1 = %d\n"  , i0 & i1);
    printf("i0 & i1 = %u\n"  , i0 & i1);
    printf("i0 & i1 = 0x%x\n", i0 & i1);

    printf("i0 - i1 = %d\n"  , i0 - i1);
    printf("i0 - i1 = %u\n"  , i0 - i1);
    printf("i0 - i1 = 0x%x\n", i0 - i1);

    return 0;
}
