#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
    printf("INT_MAX     = %d\n", INT_MAX);
    printf("INT_MIN     = %d\n", INT_MIN);
    printf("LONG_MAX    = %ld\n", LONG_MAX);
    printf("LONG_MIN    = %ld\n", LONG_MIN);

    printf("UINT_MAX    = %u\n", UINT_MAX);
    printf("ULONG_MAX   = %lu\n", ULONG_MAX);

    return EXIT_SUCCESS;
}
