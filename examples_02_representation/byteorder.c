#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <arpa/inet.h>

#define INT_BYTES sizeof(int32_t)

int
main(int argc, char *argv[])
{
    union byte_t {
    	int32_t integer;
    	uint8_t string[INT_BYTES];
    } byte;

    for (int i = 1; i < argc; i++) {
	byte.integer = atoi(argv[i]);

	for (int c = 0; c < INT_BYTES; c++) {
	    printf("%d: %02x\n", c, byte.string[c]);
	}

	putchar('\n');

	byte.integer = htonl(byte.integer);
	
	for (int c = 0; c < INT_BYTES; c++) {
	    printf("%d: %02x\n", c, byte.string[c]);
	}
    }

    return EXIT_SUCCESS;
}
