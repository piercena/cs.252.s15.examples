#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
    printf("types              | bytes\n");
    printf("char               = %lu\n", sizeof(char));
    printf("unsigned char      = %lu\n", sizeof(unsigned char));

    printf("int                = %lu\n", sizeof(int));
    printf("unsigned int       = %lu\n", sizeof(unsigned int));

    printf("short              = %lu\n", sizeof(short));
    printf("unsigned short     = %lu\n", sizeof(unsigned short));

    printf("long               = %lu\n", sizeof(long));
    printf("unsigned long      = %lu\n", sizeof(unsigned long));

    printf("long long          = %lu\n", sizeof(long long));
    printf("unsigned long long = %lu\n", sizeof(unsigned long long));

    printf("size_t             = %lu\n", sizeof(size_t));
    printf("float              = %lu\n", sizeof(float));
    printf("double             = %lu\n", sizeof(double));
    printf("long double        = %lu\n", sizeof(long double));

    return EXIT_SUCCESS;
}
