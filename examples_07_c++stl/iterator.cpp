#include <ctime>
#include <algorithm>
#include <deque>
#include <iostream>
#include <iterator>
#include <list>
#include <vector>

using namespace std;

#define MAX_ITEMS 8

int
main(int argc, char *argv[])
{
    vector<int> v;
    deque<int>  d;
    list<int>   l;

    srand(time(NULL));

    /* Add random numbers to vector */
    for (int i = 0; i < MAX_ITEMS; i++) {
    	v.push_back(rand() % MAX_ITEMS);
    }
    
    /* Print vector using index */
    cout << "Index" << endl;
    for (size_t i = 0; i < v.size(); i++) {
    	cout << v[i] << endl;
    }

    /* Print vector using iterator */
    cout << "Iterator" << endl;
    for (vector<int>::iterator it = v.begin(); it != v.end(); it++) {
    	cout << *it << endl;
    }
    
    /* Print vector using iterator (auto) */
    cout << "Iterator (auto)" << endl;
    for (auto it = v.begin(); it != v.end(); it++) {
    	cout << *it << endl;
    }
    
    /* Print vector using iterator (range) */
    cout << "Iterator (range)" << endl;
    for (auto i: v) {
    	cout << i << endl;
    }
    
    /* Print vector using iterator (auto) */
    cout << "Iterator (lambda)" << endl;
    for_each(v.begin(), v.end(), [](int i){ cout << i << endl; });
    
    /* Print vector using iterator (auto) */
    cout << "Iterator (auto, reversed)" << endl;
    for (auto it = v.rbegin(); it != v.rend(); it++) {
    	cout << *it << endl;
    }

    /* Copy and print to deque */
    cout << "Copy evens to deque" << endl;
    copy_if(v.begin(), v.end(), back_inserter(d), [](int i){ return i % 2; });
    for_each(d.begin(), d.end(), [](int i){ cout << i << endl; });
    
    /* Copy and count values in list */
    cout << "Count items in list " << endl;
    copy(v.begin(), v.end(), back_inserter(l));
    for (int i = 0; i < MAX_ITEMS; i++) {
    	cout << i << ": " << count(l.begin(), l.end(), i) << endl;
    }
    
    return EXIT_SUCCESS;
}
