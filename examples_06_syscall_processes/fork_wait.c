/* fork_wait.c: fork, wait */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <unistd.h>

static bool IsChildDone = false;

void
handle_child(int signum)
{
    pid_t pid;
    int status;

    pid = wait(&status);
    if (pid < 0) {
	perror("wait");
    }

    printf("[PARENT] pid=%d, status=%d\n", pid, WEXITSTATUS(status));
    IsChildDone = true;
}

int
main(int argc, char *argv[])
{
    pid_t pid;

    /* Register SIGCHLD handler */
    signal(SIGCHLD, handle_child);

    pid = fork();
    switch (pid) {
	/* Error */
	case -1:
	    perror("fork");
	    exit(EXIT_FAILURE);
	    break;

	/* Child */
	case 0:
	    for (int i = 0; i < 5; i++) {
		printf("[CHILD] pid=%d, i=%d\n", getpid(), i);
		sleep(1);
	    }
	    _exit(EXIT_SUCCESS);
	    break;

	/* Parent */
	default:
	    while (!IsChildDone) {
		printf("[PARENT] pid=%d (waiting...)\n", pid);
		sleep(1);
	    }
	    printf("[PARENT] pid=%d (gotchu!)\n", pid);
	    break;
    }

    return EXIT_SUCCESS;
}

