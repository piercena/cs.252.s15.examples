/* fork_exec: fork and exec */

#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    pid_t pid;
    int status;

    pid = fork();
    switch (pid) {
	/* Error */
	case -1:
	    perror("fork");
	    exit(EXIT_FAILURE);
	    break;

	/* Child */
	case 0:
	    printf("[CHILD] pid=%d\n", getpid());
	    execlp("echo", "echo", "hello", "world", NULL);
	    _exit(EXIT_FAILURE);
	    break;

	/* Parent */
	default:
	    pid = waitpid(pid, &status, 0);
	    if (pid < 0) {
		perror("waitpid");
	    }
	    printf("[PARENT] pid=%d, status=%d\n", pid, WEXITSTATUS(status));
	    break;
    }

    return (EXIT_SUCCESS);
}
