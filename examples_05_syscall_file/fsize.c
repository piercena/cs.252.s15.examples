/* fsize.c: print file size */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(int argc, char *argv[])
{
    size_t ntotal;
    size_t nread;
    FILE  *rfs;
    char   buffer[BUFSIZ];

    for (int i = 1; i < argc; i++) {
    	ntotal = 0;
	rfs    = fopen(argv[i], "r");
	if (rfs == NULL) {
	    fprintf(stderr, "cannot open %s: %s\n", argv[i], strerror(errno));
	    exit(EXIT_FAILURE);
	}

	while ((nread = fread(buffer, sizeof(char), BUFSIZ, rfs)) > 0) {
	    ntotal += nread;
	}
    	
    	fclose(rfs);

	printf("%s %lu\n", argv[i], ntotal);
    }

    return (EXIT_SUCCESS);
}
