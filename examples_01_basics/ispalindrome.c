/* ispalindrome.c */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool
is_palindrome(const char *s)
{
    size_t front = 0;
    size_t back  = strlen(s) - 1;

    while (front < back) {
    	if (s[front] != s[back]) {
    	    return false;
	}

	front++;
	back--;
    }

    return true;
}

int
main(int argc, char *argv[])
{
    char buffer[BUFSIZ];

    while (fgets(buffer, BUFSIZ, stdin) != NULL) {
    	buffer[strlen(buffer) - 1] = '\0';

    	if (is_palindrome(buffer)) {
    	    puts("Yes!");
	} else {
    	    puts("No!");
	}
    }

    return EXIT_SUCCESS;
}
