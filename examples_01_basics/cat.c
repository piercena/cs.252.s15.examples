/* cat.c */

#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
    char buffer[BUFSIZ];

    while (fgets(buffer, BUFSIZ, stdin) != NULL) {
    	fputs(buffer, stdout);
    }

    return EXIT_SUCCESS;
}
