#!/bin/sh

N=${1:-100}

for i in $(seq 1 ${N}); do
    echo $((${RANDOM} % ${N})) $((${RANDOM} % ${N}))
done
