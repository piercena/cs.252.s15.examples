#ifndef __VECTOR_H__
#define __VECTOR_H__

#include <stdlib.h>

#define DEFAULT_CAPACITY    8

struct vector_t {
    size_t size;
    size_t capacity;
    int   *data;
};

struct vector_t *vector_create(size_t size, size_t capacity);
void             vector_delete(struct vector_t *v);

int		 vector_get(struct vector_t *v, size_t index);
void		 vector_set(struct vector_t *v, size_t index, int value);

void		 vector_append(struct vector_t *v, int value);

void		 vector_print(struct vector_t *v);

#endif
